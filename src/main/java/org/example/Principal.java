package org.example;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class Principal {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public static void main(String[] args) {
        ArrayList<Funcionario> funcionarios = addFuncionarios();
        funcionarios.removeIf(funcionario -> funcionario.getNome().equals("João"));
        imprimirFuncionarios(funcionarios);
        aumentarSalario(10, funcionarios);
        Map<String, List<Funcionario>> funcionariosMapPorFuncao = addMapFuncionarios(funcionarios);
        imprimirFuncionariosPorFuncao(funcionariosMapPorFuncao);
        imprimirAniversariantes(funcionarios, 10, 12);
        imprimirFuncionarioMaiorIdade(funcionarios);
        imprimirFuncionariosOrderAlfabetica(funcionarios);
        imprimirTotalSalarios(funcionarios);
        imprimirQuantidadesSalariosMinimos(funcionarios);

    }

    public static ArrayList<Funcionario> addFuncionarios() {
        ArrayList<Funcionario> funcionarios = new ArrayList<>();

        funcionarios.add(new Funcionario(BigDecimal.valueOf(2009.44), "Operador", "Maria", formataStringLocalDate("18/10/2000")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(2284.38), "Operador", "João", formataStringLocalDate("12/05/1990")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(9836.14), "Coordenador", "Caio", formataStringLocalDate("02/05/1961")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(19119.88), "Diretor", "Miguel", formataStringLocalDate("14/10/1998")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(2234.68), "Recepicionista", "Alice", formataStringLocalDate("05/01/1995")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(1582.72), "Operador", "Heitor", formataStringLocalDate("19/11/1999")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(4071.84), "Contador", "Arthur", formataStringLocalDate("31/03/1993")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(3017.45), "Gerente", "Laura", formataStringLocalDate("08/07/1994")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(1606.45), "Eletricista", "Heloísa", formataStringLocalDate("24/05/2003")));
        funcionarios.add(new Funcionario(BigDecimal.valueOf(2799.93), "Gerente", "Helena", formataStringLocalDate("02/09/1996")));

        return funcionarios;
    }

    public static void aumentarSalario(int porcentagem, ArrayList<Funcionario> funcionarios) {
        funcionarios.forEach(funcionario -> funcionario.setSalario(funcionario.getSalario().add(funcionario.getSalario().multiply(BigDecimal.valueOf(porcentagem/100.0)))));
    }

    public static Map<String, List<Funcionario>> addMapFuncionarios(List<Funcionario> funcionarios) {
        Map<String, List<Funcionario>> funcionariosMapPorFuncao = funcionarios.stream()
                .collect(Collectors.groupingBy(Funcionario::getFuncao));
        return funcionariosMapPorFuncao;
    }

    private static void imprimirFuncionarios(List<Funcionario> funcionarios) {
        funcionarios.forEach(funcionario -> {
            System.out.print(funcionario.getNome());
            System.out.print("      " );
            System.out.print(formataLocalDate(funcionario.getDataNascimento()));
            System.out.print("      " );
            System.out.print(formatarValor(funcionario.getSalario()));
            System.out.print("      " );
            System.out.print(funcionario.getFuncao());
            System.out.println();
        });
    }

    public static void imprimirFuncionariosPorFuncao(Map<String, List<Funcionario>> funcionariosPorFuncao) {
        System.out.println();
        System.out.println("Funcionários por função:");
        funcionariosPorFuncao.forEach((funcao, funcionarios) -> {
            System.out.println("Função: " + funcao);
            imprimirFuncionarios(funcionarios);
        });
    }

    public static void imprimirAniversariantes(List<Funcionario> funcionarios, int... meses) {
        List<Funcionario> aniversariantes =
                funcionarios.stream().filter(funcionario -> Arrays.stream(meses).anyMatch(mes -> funcionario.getDataNascimento().getMonthValue() == mes)).toList();
        System.out.println();
        System.out.println("Funcionários que fazem aniversário nos meses " + Arrays.toString(meses) + ":");
        imprimirFuncionarios(aniversariantes);
    }

    public static void imprimirFuncionarioMaiorIdade(List<Funcionario> funcionarios) {
        System.out.println("Funcionário com maior idade:");
        Funcionario funcionarioMaiorIdade = Collections.max(funcionarios, Comparator.comparing(funcionario ->
                ChronoUnit.YEARS.between(funcionario.getDataNascimento(), LocalDate.now())));

        System.out.print(funcionarioMaiorIdade.getNome());
        System.out.print("      Idade: " );
        System.out.print(calcularIdade(funcionarioMaiorIdade.getDataNascimento()));

        System.out.println();
    }

    private static void imprimirFuncionariosOrderAlfabetica(List<Funcionario> funcionarios) {
        Collections.sort(funcionarios, Comparator.comparing(Funcionario::getNome));
        System.out.println();
        System.out.println("Funcionários em ordem alfabetica ");
        imprimirFuncionarios(funcionarios);

    }

    private static void imprimirTotalSalarios(List<Funcionario> funcionarios) {
        System.out.println("Total dos salários:");
        BigDecimal total = funcionarios.stream()
                .map(Funcionario::getSalario)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(formatarValor(total));
    }

    public static void imprimirQuantidadesSalariosMinimos(List<Funcionario> funcionarios) {
        System.out.println("Quantidade de salários mínimos:");
        BigDecimal salarioMinimo = new BigDecimal("1212.00");
        funcionarios.forEach(funcionario -> {
            BigDecimal quantidadeSalariosMinimos = funcionario.getSalario().divide(salarioMinimo, 2, RoundingMode.DOWN);
            System.out.print(funcionario.getNome());
            System.out.print(String.format("%s Salários", quantidadeSalariosMinimos));
            System.out.println();
        });
    }
    private static int calcularIdade(final LocalDate aniversario) {
        return Period.between(aniversario, LocalDate.now()).getYears();
    }

    private static LocalDate formataStringLocalDate(String data) {
        return LocalDate.parse(data, formatter);
    }

    private static String formataLocalDate(LocalDate data) {
        return data.format(formatter);
    }

    private static String formatarValor(BigDecimal valor) {
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        return decimalFormat.format(valor);
    }
}